import qbs 1.0
import qbs.Environment

Project {
    name: "CuteVNC"

    property bool buildTests: false
    property bool enableCoverage: false

    property string packageName: "it.mardy.lomiri-vnc"
    property string version: "0.2"

    qbsSearchPaths: "qbs"

    QtGuiApplication {
        name: "CuteVNC"
        version: project.version
        install: true

        cpp.cxxLanguageVersion: "c++14"
        cpp.defines: [
            'QT_DISABLE_DEPRECATED_BEFORE=0x050900',
            'QT_DEPRECATED_WARNINGS=0',
        ]
        cpp.enableExceptions: false

        Group {
            prefix: "src/"
            files: [
                "key_mapping.h",
                "main.cpp",
                "scaler.cpp",
                "scaler.h",
                "types.cpp",
                "types.h",
                "vnc_client.cpp",
                "vnc_client.h",
                "vnc_output.cpp",
                "vnc_output.h",
            ]
        }

        Group {
            prefix: "src/desktop/"
            files: [
                "qml/ui.qrc",
            ]
        }

        Group {
            prefix: "data/"
            files: [
                "icons/icons.qrc",
                "lomiri_vnc.desktop",
            ]
        }

        Group {
            files: "data/lomiri_vnc.svg"
            fileTags: "freedesktop.appIcon"
        }

        Depends { name: "cpp" }
        Depends { name: "Qt.core" }
        Depends { name: "Qt.quick" }
        Depends { name: "Qt.quickcontrols2" }
        Depends { name: "Qt.svg" }
        Depends { name: "freedesktop" }
        Depends { name: "libvncclient" }

        freedesktop.name: product.name

        /*
         * Ubuntu Touch specific configuration
         */
        Depends {
            name: "ubuntutouch"
            condition: Environment.getEnv("TARGET_SYSTEM") == "UbuntuTouch"
        }

        Properties {
            condition: ubuntutouch.present
            freedesktop.desktopKeys: ubuntutouch.desktopKeys
            ubuntutouch.overrideDesktopKeys: ({
                "Icon": "./share/icons/hicolor/scalable/apps/lomiri_vnc.svg",
                "Version": "1.0",
            })
            ubuntutouch.manifest: ({
                "version": project.version + "-0"
            })
        }

        Group {
            condition: ubuntutouch.present
            prefix: "data/ubuntu-touch/"
            files: [
                "manifest.json",
                "lomiri_vnc.apparmor",
                "lomiri_vnc.url-dispatcher",
            ]
        }

        Group {
            condition: ubuntutouch.present
            prefix: "src/ubuntu-touch/"
            files: [
                "qml/ubuntu-touch.qrc",
            ]
        }
    }

    references: [
        "tests/tests.qbs",
    ]

    AutotestRunner {
        name: "check"
        Depends { productTypes: ["coverage-clean"] }
    }
}
