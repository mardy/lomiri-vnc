import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

RowLayout {
    id: root

    signal oskRequested()
    signal toggleFullScreenRequested()
    signal moveViewRequested()

    anchors.fill: parent

    OsdButton {
        iconSource: "qrc:/icons/keyboard"
        onClicked: root.oskRequested()
    }

    Item { Layout.fillWidth: true }

    OsdButton {
        Layout.alignment : Qt.AlignRight
        iconSource: "qrc:/icons/move-view"
        onClicked: root.moveViewRequested()
    }

    OsdButton {
        Layout.alignment : Qt.AlignRight
        iconSource: "qrc:/icons/fullscreen"
        onClicked: root.toggleFullScreenRequested()
    }
}
