import QtQuick 2.7
import QtQuick.Controls 2.2

ToolButton {
    id: root

    property alias iconSource: image.source

    contentItem: Image {
        id: image
    }
}
