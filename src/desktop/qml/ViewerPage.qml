import LomiriVNC 1.0
import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Window 2.2

Page {
    id: root

    property alias vncClient: viewer.client

    title: qsTr("VNC Client - Viewer")

    ZoomPanHandler {
        id: zoomPanHandler
        anchors {
            left: parent.left; right: parent.right;
            top: parent.top; bottom: keyboardRectangle.top
        }
        viewer: viewer

        VncOutput {
            id: viewer
            anchors.fill: parent
        }
    }

    Drawer {
        id: drawer
        width: root.width
        edge: Qt.BottomEdge

        OsdRow {
            onOskRequested: { drawer.close(); root.showKeyboard() }
            onToggleFullScreenRequested: {
                drawer.close();
                root.toggleFullScreen()
            }
            onMoveViewRequested: { drawer.close(); root.enableMoveViewMode() }
        }
    }

    KeyboardRectangle {
        id: keyboardRectangle
    }

    function toggleFullScreen() {
        var window = Window.window
        if (window.visibility == Window.FullScreen) {
            window.showNormal()
        } else {
            window.showFullScreen()
        }
    }

    function showKeyboard() {
        viewer.forceActiveFocus()
        Qt.inputMethod.show()
    }

    function enableMoveViewMode() {
        zoomPanHandler.moveViewMode = true
    }
}
