import LomiriVNC 1.0
import QtQml 2.2
import QtQuick 2.7
import QtQuick.Controls 2.2

ApplicationWindow {
    id: root

    width: 640
    height: 480
    visible: true

    PlatformInitialization {}

    StackView {
        id: pageStack
        anchors.fill: parent
        initialItem: mainPageComponent
    }

    Component {
        id: mainPageComponent
        MainPage {
            onConnectionRequested: {
                vncClient.connectToServer(host, password);
            }
        }
    }

    VncClient {
        id: vncClient

        onConnectedChanged: if (connected) {
            pageStack.push(Qt.resolvedUrl("ViewerPage.qml"), {
                "vncClient": vncClient
            })
        }
    }
}
