import QtQuick 2.7

Item {
    id: root

    anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
    height: Qt.inputMethod.keyboardRectangle.height
}
