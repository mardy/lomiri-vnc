import QtQuick 2.7

Item {
    id: root

    default property alias content: contentItem.data
    property bool moveViewMode: false
    property var viewer: null

    PinchArea {
        property real initialScale: 1.0
        property point initialCenter

        anchors.fill: parent
        onPinchStarted: {
            initialScale = viewer.scale
            initialCenter = viewer.center
        }
        onPinchUpdated: {
            var requestedScale = initialScale * pinch.scale
            var diffX = (pinch.center.x - pinch.startCenter.x) /
                requestedScale
            var diffY = (pinch.center.y - pinch.startCenter.y) /
                requestedScale
            viewer.requestedScale = requestedScale
            viewer.center = Qt.point(initialCenter.x - diffX, initialCenter.y - diffY)
        }

        Item {
            id: contentItem
            anchors.fill: parent
            enabled: !root.moveViewMode && !parent.pinch.active
        }

        Flickable {
            id: flickable

            property real __startCenterX: 0
            property real __startCenterY: 0
            property real __startContentX: 0
            property real __startContentY: 0

            anchors.fill: parent
            contentWidth: viewer.remoteScreenSize.width * viewer.scale
            contentHeight: viewer.remoteScreenSize.height * viewer.scale
            bottomMargin: viewer.bottomMargin
            leftMargin: viewer.leftMargin
            rightMargin: viewer.rightMargin
            topMargin: viewer.topMargin
            enabled: root.moveViewMode
            onContentXChanged: if (moving) updateView()
            onContentYChanged: if (moving) updateView()
            onMovementStarted: updateOffset()
            onMovementEnded: updateContentPosition()

            Connections {
                target: viewer
                enabled: !flickable.moving
                onCenterChanged: flickable.updateContentPosition()
                onScaleChanged: flickable.updateContentPosition()
            }

            function updateContentPosition() {
                var topLeft = viewer.vncToItem(Qt.point(0, 0))
                contentX = -topLeft.x
                contentY = -topLeft.y
                updateOffset()
            }

            function updateOffset() {
                __startContentX = contentX
                __startContentY = contentY
                __startCenterX = viewer.center.x
                __startCenterY = viewer.center.y
            }

            function updateView() {
                var s = viewer.scale
                var cx = (contentX - __startContentX) / s + __startCenterX
                var cy = (contentY - __startContentY) / s + __startCenterY
                viewer.center = Qt.point(cx, cy)
            }

            /* We keep this here for debugging
            Rectangle {
                color: "red"
                opacity: 0.5
                width: viewer.remoteScreenSize.width * viewer.scale
                height: viewer.remoteScreenSize.height * viewer.scale
                Rectangle {
                    width: parent.width / 2
                    height: parent.height / 2
                    color: "blue"
                    anchors.centerIn: parent
                }
            }
            */
        }
    }

    OsdButton {
        anchors { right: parent.right; bottom: parent.bottom }
        visible: root.moveViewMode
        iconSource: "qrc:/icons/stop-move-view"
        onClicked: root.moveViewMode = false
    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.NoButton
        onWheel: {
            console.log("Angle delta:" + wheel.angleDelta.y)
            viewer.requestedScale = viewer.scale + wheel.angleDelta.y / 120 / 20
        }
    }
}
