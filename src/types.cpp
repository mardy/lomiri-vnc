/*
 * Copyright (C) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LomiriVNC.
 *
 * LomiriVNC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LomiriVNC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LomiriVNC.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "types.h"

#include "vnc_client.h"
#include "vnc_output.h"

#include <QDebug>
#include <QQmlEngine>

using namespace LomiriVNC;

void LomiriVNC::registerTypes()
{
    qmlRegisterType<VncClient>("LomiriVNC", 1, 0, "VncClient");
    qmlRegisterType<VncOutput>("LomiriVNC", 1, 0, "VncOutput");
}
