import qbs 1.0

Project {
    condition: project.buildTests

    Test {
        name: "scaler-test"
        files: [
            "../src/scaler.cpp",
            "../src/scaler.h",
            "tst_scaler.cpp",
        ]
    }

    CoverageClean {
        condition: project.enableCoverage
    }

    CoverageReport {
        condition: project.enableCoverage
        extractPatterns: [ '*/src/*.cpp' ]
    }
}
